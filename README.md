# Converssistant

AI assistant for your messengers(TG only at this moment)

Uses `@mtproto/core` to listen telegram events and `langchain` to process requests to LLM's

# TODO
- Better LLM management (allow user to build own chains)
- Better prompt management
- Add/remove allowed users from directly messenger(with command) and via GUI
- Make assistant more 'rogue', it must assist in dialog by itself, without requirement to call it directly with command

## Scripts

Watch `package.json` for dev scripts

