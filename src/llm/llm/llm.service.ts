import { Injectable, Logger } from '@nestjs/common';
import {
  ConversationChain,
  LLMChain,
  OpenAIModerationChain,
} from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import {
  ChatPromptTemplate,
  HumanMessagePromptTemplate,
  SystemMessagePromptTemplate,
  MessagesPlaceholder,
  PromptTemplate,
} from 'langchain/prompts';
import { BufferMemory } from 'langchain/memory';
import { OpenAI } from 'langchain/llms/openai';
import { ConfigService } from 'src/config/config/config.service';

@Injectable()
export class LlmService {
  private readonly logger = new Logger(LlmService.name);

  constructor(private readonly configService: ConfigService) {}

  createRap = async (person: string) => {
    const raper = new OpenAI({
      temperature: 0.7,
      timeout: 1000 * 60,
      maxConcurrency: 2,
    });
    const prompt = PromptTemplate.fromTemplate(
      'Please write 2-3 quatrains for a gangsta-style rap song. Use the style of {style}. Use the most suitable rhymes. Use literary stylistic devices. Use profanity.',
    );
    const chainA = new LLMChain({ llm: raper, prompt });

    const res = await chainA.call({ style: person });
    console.log(res);
    return res.text;
  };

  sendToLlm = async (input: string) => {
    const moderation = new OpenAIModerationChain();
    let badInput = true;
    try {
      // Send the user's input to the moderation chain and wait for the result
      const { output: badResult } = await moderation.call({
        input: input,
        throwError: true, // If set to true, the call will throw an error when the moderation chain detects violating content. If set to false, violating content will return "Text was found that violates OpenAI's content policy.".
      });
      console.log(badResult);
      badInput = false;
    } catch (err) {
      console.warn('User spels bad words', input);
      badInput = true;
    }
    if (badInput) {
      return { response: 'Не ругайся!' };
    }
    const chat = new ChatOpenAI({ temperature: 0 });

    const assistantName = this.configService.getAssistantName();

    const chatPrompt = ChatPromptTemplate.fromPromptMessages([
      SystemMessagePromptTemplate.fromTemplate(
        `The following is a friendly conversation between a human and an AI. The AI is talkative and provides lots of specific details from its context. If the AI does not know the answer to a question, it truthfully says it does not know. AI's name is ${assistantName}`,
      ),
      new MessagesPlaceholder('history'),
      HumanMessagePromptTemplate.fromTemplate('{input}'),
    ]);

    const chain = new ConversationChain({
      memory: new BufferMemory({ returnMessages: true, memoryKey: 'history' }),
      prompt: chatPrompt,
      llm: chat,
    });

    const response = await chain.call({
      input,
    });

    return response;
  };
}
