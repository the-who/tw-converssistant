import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tg_code')
export class TgCode {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'code' })
  code: string;
}
