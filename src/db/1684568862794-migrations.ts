import { MigrationInterface, QueryRunner } from 'typeorm';

export class Migrations1684568862794 implements MigrationInterface {
  name = 'Migrations1684568862794';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "tg_user" ("id" varchar PRIMARY KEY NOT NULL, "user_id" varchar NOT NULL, "phone" varchar NOT NULL, "user_name" varchar NOT NULL, "accessHash" varchar NOT NULL, "isAllowed" boolean NOT NULL DEFAULT (1))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_9245905c01d98a6cdb11ceebb8" ON "tg_user" ("user_id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tg_code" ("id" varchar PRIMARY KEY NOT NULL, "code" varchar NOT NULL)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "tg_code"`);
    await queryRunner.query(`DROP INDEX "IDX_9245905c01d98a6cdb11ceebb8"`);
    await queryRunner.query(`DROP TABLE "tg_user"`);
  }
}
