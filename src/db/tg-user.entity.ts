import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index(['userId'])
@Entity('tg_user')
export class TgUser {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'user_id' })
  userId: string;

  @Column({ name: 'phone' })
  phone: string;

  @Column({ name: 'user_name' })
  userName: string;

  @Column({ name: 'accessHash' })
  accessHash: string;

  @Column({ name: 'isAllowed', default: true })
  isAllowed: boolean;
}
