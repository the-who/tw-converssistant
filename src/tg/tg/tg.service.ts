import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { ConfigService } from 'src/config/config/config.service';
import MTProto from '@mtproto/core';
import path from 'path';
import { generateWord } from 'src/utils/random-words';
import { LlmService } from 'src/llm/llm/llm.service';
import { appendFile } from 'fs/promises';
import { sleep } from '@mtproto/core/src/utils/common';
import { TgCodeService } from 'src/tg-code/tg-code/tg-code.service';
import { TgUserService } from 'src/tg-user/tg-user/tg-user.service';
// const prompts = require('prompts');

export enum PersonalTgCommands {
  rap = 'ща зачитаю репчик как ',
  save = 'надо сохранить',
  randomWord = 'рандом слово',
}

// export async function getCode() {
//   // you can implement your code fetching strategy here
//   return (
//     await prompts({
//       type: 'text',
//       name: 'code',
//       message: 'Enter the code sent:',
//     })
//   ).code;
// }

@Injectable()
export class TgService implements OnApplicationBootstrap {
  private readonly logger = new Logger(TgService.name);

  private readonly mtproto = new MTProto({
    api_id: this.configService.getTgApiId(),
    api_hash: this.configService.getTgApiHash(),

    storageOptions: {
      path: path.resolve(__dirname, '../data/1.json'),
    },
  });

  private allowedUsers: string[] = [];
  private phone: string = this.configService.getTgPhone();
  private password: string = this.configService.getTgPassword();

  constructor(
    private readonly configService: ConfigService,
    private readonly llmService: LlmService,
    private readonly tgCodeService: TgCodeService,
    private readonly tgUserService: TgUserService,
  ) {}

  async onApplicationBootstrap() {
    const allowedUsers = await this.tgUserService.getAllowedUsers();
    this.allowedUsers = allowedUsers.map((u) => u.userId);
    this.mtproto.updates.on('updatesTooLong', (updateInfo: any) => {
      this.logger.log('updatesTooLong:', updateInfo);
    });

    this.mtproto.updates.on('updateShortMessage', async (updateInfo: any) => {
      if (updateInfo._ !== 'updateShortMessage') {
        this.logger.log('Unknown updateShortMessage', updateInfo);
        return;
      }
      const userId = updateInfo.user_id;

      if (!this.allowedUsers.includes(userId)) return;
      const { message } = updateInfo;
      // Handle my own messages
      if (updateInfo.out === true) {
        const msg: string = updateInfo.message;
        if (msg.startsWith(PersonalTgCommands.rap)) {
          const person = msg.split('как ')[1];
          const result = await this.llmService.createRap(person);

          await this.sendMessageToUser({
            userId,
            msg: result,
          });
          return;
        }
        if (updateInfo.message === PersonalTgCommands.randomWord) {
          const message = generateWord();
          await this.sendMessageToUser({
            userId,
            msg: message,
          });
        }

        // handle my reply msgs
        if (updateInfo.reply_to) {
          // save messages content somewhere
          if (msg.includes(PersonalTgCommands.save)) {
            const msgIdToFetch = updateInfo.reply_to.reply_to_msg_id;
            const message = await this.fetchMessage(msgIdToFetch);
            const content = message.message;
            const dbPath = process.env.DB_FILE_PATH;
            if (!dbPath) {
              await this.sendMessageToUser({
                userId,
                msg: 'Не удалось сохранить сообщение, так как нет пути к файлу с БД',
              });
              return;
            }
            await appendFile(
              dbPath,
              `\nUSER_ID:${userId}MSG_ID:${msgIdToFetch}\n${content}\n`,
            );
            await this.sendMessageToUser({
              userId,
              msg: 'Сохранил это сообщение в БД',
              replyTo: msgIdToFetch,
            });
            return;
          }
        }

        this.logger.log(updateInfo);
        return;
      }
      // Handle other messages
      const assistantName = this.configService.getAssistantName();
      const startsWithMyName = message.startsWith(`${assistantName}, `);
      const doProcessWithLlm = startsWithMyName;
      if (!doProcessWithLlm) {
        this.logger.log(updateInfo);
        return;
      }

      const llmResponse = await this.llmService.sendToLlm(message);
      this.logger.log(llmResponse);

      await this.sendMessageToUser({
        userId,
        msg: llmResponse.response,
      });
    });

    this.mtproto.updates.on('updateShortChatMessage', (updateInfo: any) => {
      this.logger.log('updateShortChatMessage:', updateInfo);
    });

    this.mtproto.updates.on('updateShort', async (updateInfo: any) => {
      const unwantedUpdates = [
        'updateMessagePoll',
        'updateDeleteChannelMessages',
        'updateChannelUserTyping',
        'updateChannelMessageViews',
      ];
      if (updateInfo.update) {
        const updateType = updateInfo.update._;
        if (unwantedUpdates.includes(updateType)) return;

        if (updateInfo.update._ === 'updateUserTyping') {
          const userId = updateInfo.update.user_id;

          if (!this.allowedUsers.includes(userId)) return;

          // await this.sendMessageToUser({
          //   userId, msg: 'I see you typing now...'
          // })
        }
      }
      this.logger.log('updateShort:', updateInfo);
    });

    this.mtproto.updates.on('updatesCombined', (updateInfo: any) => {
      this.logger.log('updatesCombined:', updateInfo);
    });

    this.mtproto.updates.on('updates', ({ updates }: any) => {
      const upd: any[] = updates
        .filter((u: any) => u._ !== 'updateEditChannelMessage')
        .filter((u: any) => u._ !== 'updateNewChannelMessage')
        .filter((u: any) => u._ !== 'updateMessagePoll');
      if (upd.length > 0) {
        this.logger.log(upd);
      }
    });

    this.mtproto.updates.on('updateShortSentMessage', (updateInfo: any) => {
      this.logger.log('updateShortSentMessage:', updateInfo);
    });
  }

  async fetchMessage(msgId: number) {
    const result = await this.callTg('messages.getMessages', {
      id: [
        {
          _: 'inputMessageID',
          id: msgId,
        },
      ],
    });

    const [message] = result.messages;
    return message;
  }

  async getUserByUsername(username: string) {
    const userInfo = await this.callTg('contacts.resolveUsername', {
      username,
    });
    const [user] = userInfo.users;
    return user;
  }

  async getUser() {
    try {
      const user = await this.callTg('users.getFullUser', {
        id: {
          _: 'inputUserSelf',
        },
      });

      return user;
    } catch (error) {
      return null;
    }
  }

  async sendCode(phone: string) {
    return this.callTg('auth.sendCode', {
      phone_number: phone,
      settings: {
        _: 'codeSettings',
      },
    });
  }

  async signIn({ code, phone, phone_code_hash }: any) {
    return this.callTg('auth.signIn', {
      phone_code: code,
      phone_number: phone,
      phone_code_hash: phone_code_hash,
    });
  }

  async signUp({ phone, phone_code_hash }: any) {
    return this.callTg('auth.signUp', {
      phone_number: phone,
      phone_code_hash: phone_code_hash,
      first_name: 'MTProto',
      last_name: 'Core',
    });
  }

  async getPassword() {
    return this.callTg('account.getPassword');
  }

  async checkPassword({ srp_id, A, M1 }: any) {
    return this.callTg('auth.checkPassword', {
      password: {
        _: 'inputCheckPasswordSRP',
        srp_id,
        A,
        M1,
      },
    });
  }

  async auth() {
    const user = await this.getUser();
    this.logger.log(user);

    const phone = this.phone;

    if (!user) {
      const { phone_code_hash } = await this.sendCode(phone);
      const code = await this.tgCodeService.getCode();

      try {
        const signInResult = await this.signIn({
          code,
          phone,
          phone_code_hash,
        });

        if (signInResult._ === 'auth.authorizationSignUpRequired') {
          await this.signUp({
            phone,
            phone_code_hash,
          });
        }
      } catch (error: any) {
        if (error.error_message !== 'SESSION_PASSWORD_NEEDED') {
          this.logger.log(`error:`, error);

          return;
        }

        // 2FA

        const password = this.password;

        const { srp_id, current_algo, srp_B } = await this.getPassword();
        const { g, p, salt1, salt2 } = current_algo;

        const { A, M1 } = await this.mtproto.crypto.getSRPParams({
          g,
          p,
          salt1,
          salt2,
          gB: srp_B,
          password,
        });

        const checkPasswordResult = await this.checkPassword({ srp_id, A, M1 });
        this.logger.log(checkPasswordResult);
      }
    }
  }

  async sendMessageToUser(params: {
    userId: string;
    msg: string;
    replyTo?: number;
  }) {
    const { userId, msg, replyTo } = params;
    const user = await this.tgUserService.getUserByTgId(userId);
    if (!user) {
      this.logger.warn(
        `Can not send message to user. User ${userId} not found in DB`,
      );
      return;
    }
    const { accessHash } = user;
    const opts: any = {
      clear_draft: true,
      peer: {
        _: 'inputPeerUser',
        user_id: userId,
        access_hash: accessHash,
      },
      message: msg,
      entities: [
        {
          _: 'messageEntityBold',
          offset: 0,
          length: 13,
        },
      ],

      random_id:
        Math.ceil(Math.random() * 0xffffff) +
        Math.ceil(Math.random() * 0xffffff),
    };
    if (replyTo) {
      opts.reply_to_msg_id = replyTo;
    }
    await this.callTg('messages.sendMessage', opts);
  }

  async callTg(method: any, params?: any, options: any = {}): Promise<any> {
    try {
      const result = await this.mtproto.call(method, params, options);

      return result;
    } catch (error: any) {
      this.logger.log(`${method} error:`, error);

      const { error_code, error_message } = error;

      if (error_code === 420) {
        const seconds = Number(error_message.split('FLOOD_WAIT_')[1]);
        const ms = seconds * 1000;

        await sleep(ms);

        return this.callTg(method, params, options);
      }

      if (error_code === 303) {
        const [type, dcIdAsString] = error_message.split('_MIGRATE_');

        const dcId = Number(dcIdAsString);

        // If auth.sendCode call on incorrect DC need change default DC, because
        // call auth.signIn on incorrect DC return PHONE_CODE_EXPIRED error
        if (type === 'PHONE') {
          await this.mtproto.setDefaultDc(dcId);
        } else {
          Object.assign(options, { dcId });
        }

        return this.callTg(method, params, options);
      }

      return Promise.reject(error);
    }
  }
}
