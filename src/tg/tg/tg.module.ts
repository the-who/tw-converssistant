import { Module } from '@nestjs/common';
import { TgService } from './tg.service';
import { ConfigModule } from 'src/config/config/config.module';
import { LlmModule } from 'src/llm/llm/llm.module';
import { TgCodeModule } from 'src/tg-code/tg-code/tg-code.module';
import { TgUserModule } from 'src/tg-user/tg-user/tg-user.module';

@Module({
  imports: [ConfigModule, LlmModule, TgCodeModule, TgUserModule],
  providers: [TgService],
  exports: [TgService],
})
export class TgModule {}
