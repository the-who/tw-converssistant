import { Injectable } from '@nestjs/common';
import { config } from 'dotenv';

config();

@Injectable()
export class ConfigService {
  getTgApiHash() {
    const apiHash = this.getEnv('TG_API_HASH');
    return apiHash;
  }
  getTgApiId() {
    const apiId = this.getEnv('TG_API_ID');
    return apiId;
  }
  getTgPhone() {
    const tgPhone = this.getEnv('TG_PHONE');
    return tgPhone;
  }
  getTgPassword() {
    const tgPassword = this.getEnv('TG_PASSWORD', false);
    return tgPassword;
  }

  getAssistantName() {
    const name = this.getEnv('ASSISTANT_NAME');
    return name;
  }

  private getEnv(name: string, required = true) {
    const val = process.env[name];
    if (required && !val) {
      throw new Error(`${name} env var not found`);
    }
    return val;
  }
}
