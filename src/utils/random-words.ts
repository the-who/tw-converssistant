import { getRandom } from './random';

const MAX = 11;
const MIN = 7;
const letters: any = {
  vowels: 'аеиоуыэюя',
  consonants: 'бвгджзклмнпрстфхцчшщ',
};
const other: any = 'ёйьъ';

const getLetter = (type: any = 'consonants') => {
  const letterId = getRandom(0, letters[type].length - 1);
  return letters[type][letterId];
};

export const generateWord = () => {
  const length = getRandom(MIN, MAX);
  let lastVowel = null;
  let lastConsonant = null;
  let word = '';
  for (let i = 0; i <= length; i++) {
    if (i % 2 === 0) {
      const letter = getLetter('consonants');
      word += letter;
      lastConsonant = letter;
    } else {
      const letter = getLetter('vowels');
      word += letter;
      lastVowel = letter;
    }
  }
  return word;
};
