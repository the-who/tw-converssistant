import { Module } from '@nestjs/common';
import { TgCodeService } from './tg-code.service';
import { TgCodeController } from './tg-code.controller';
import { TgCode } from 'src/db/tg-code.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([TgCode])],
  providers: [TgCodeService],
  controllers: [TgCodeController],
})
export class TgCodeModule {}
