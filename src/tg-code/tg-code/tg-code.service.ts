import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TgCode } from 'src/db/tg-code.entity';
import { setTimeout } from 'timers/promises';
import { Repository } from 'typeorm';

@Injectable()
export class TgCodeService {
  private readonly logger = new Logger(TgCodeService.name);

  constructor(
    @InjectRepository(TgCode)
    private readonly tgCodeRepository: Repository<TgCode>,
  ) {}

  async addCode(code: string) {
    const saved = await this.tgCodeRepository.save({ code });

    return saved;
  }

  /**
   * Try to get code from DB until it is not found. When found - clear codes table
   */
  async getCode() {
    let codeVal: null | TgCode = null;
    while (true) {
      const code = await this.tgCodeRepository.find({
        take: 1,
      });
      if (code.length > 0) {
        codeVal = code[0];
        break;
      }
      this.logger.debug('Code not found in DB, waiting to fetch again');
      await setTimeout(5000);
    }

    await this.tgCodeRepository.delete({
      id: codeVal.id,
    });
    this.logger.debug('Code deleted from db');

    return codeVal.code;
  }
}
