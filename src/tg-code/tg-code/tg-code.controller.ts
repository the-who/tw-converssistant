import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { TgCodeService } from './tg-code.service';

export class AddCodeDto {
  code: string;
}

@Controller('tg-code')
export class TgCodeController {
  constructor(private readonly tgCodeService: TgCodeService) {}

  @Post('/')
  async addCode(@Body() addCodeDto: AddCodeDto) {
    try {
      const { code } = addCodeDto;
      const result = await this.tgCodeService.addCode(code);

      return result;
    } catch (err) {
      throw new HttpException('Something went wrong', HttpStatus.BAD_REQUEST);
    }
  }
}
