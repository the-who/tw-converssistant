import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TgUser } from 'src/db/tg-user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TgUserService {
  constructor(
    @InjectRepository(TgUser)
    private readonly tgUserRepository: Repository<TgUser>,
  ) {}

  async getUserByTgId(userId: string) {
    const result = await this.tgUserRepository.findOne({
      where: {
        userId,
      },
    });

    return result;
  }

  async getAllowedUsers() {
    const result = await this.tgUserRepository.find({
      where: {
        isAllowed: true,
      },
    });

    return result;
  }
}
