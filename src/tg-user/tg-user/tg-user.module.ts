import { Module } from '@nestjs/common';
import { TgUserService } from './tg-user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TgUser } from 'src/db/tg-user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TgUser])],
  providers: [TgUserService],
})
export class TgUserModule {}
