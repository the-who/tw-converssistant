import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TgModule } from './tg/tg/tg.module';
import { ConfigModule } from './config/config/config.module';
import { LlmModule } from './llm/llm/llm.module';
import { TgCodeModule } from './tg-code/tg-code/tg-code.module';
import { TgUserModule } from './tg-user/tg-user/tg-user.module';

@Module({
  imports: [TgModule, ConfigModule, LlmModule, TgCodeModule, TgUserModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
