import { join } from 'path';
import { DataSourceOptions } from 'typeorm';

export const ormconfig: DataSourceOptions = {
  type: 'sqlite',
  database: 'converssistant',
  entities: [join(__dirname, 'src/**/*.entity{.ts,.js}')],
  migrations: [join(__dirname, 'src/db/migrations/*{.ts,.js}')],
  synchronize: false,
};
